"""Api tests
"""
import json
from pathlib import Path
import sys
import pytest
from py.test import raises
from pytest_mock import mocker
import requests
from requests import Response
import getpass

testpath = Path(__file__).parent
datapath = testpath / 'data'

sys.path.append(str(testpath.parent))

from zephir_client import enables_signature, cmdline_factory, authenticate, token_file, ZcliConfig
from tiramisu_json_api import Config

URL = 'zephir2.ac-test.fr'
VERSION = 'v1'
FILENAME = '.zephir-config.yaml'
CONFIG_FILE = Path.home() / FILENAME
TOKEN = "test_token"


@pytest.fixture
def api_v1():
    with open(datapath / 'api_v1_options.json') as fh:
        jsoncontent = json.load(fh)
    return jsoncontent


@pytest.fixture
def create_zcli_config():
    yaml_template = f"""url: {URL}
version: {VERSION}"""
    with CONFIG_FILE.open("w", encoding ="utf-8") as fh:
        fh.write(yaml_template)


def gen_zconf_value(val):
    if val == "Entrez l'adresse du serveur Zephir : ":
        return "mon_zephir.com"
    else:
        return "ma_version"


def test_generate_config_file(monkeypatch):
    if CONFIG_FILE.is_file():
        CONFIG_FILE.unlink()
    monkeypatch.setattr('builtins.input', gen_zconf_value)
    zcliconf = ZcliConfig()
    assert zcliconf.url == "mon_zephir.com"
    assert zcliconf.version == "ma_version"
    with CONFIG_FILE.open("r", encoding ="utf-8") as fh:
        assert fh.read() == f"""url: mon_zephir.com
version: ma_version"""


def test_message(create_zcli_config, api_v1):
    config = Config(api_v1)
    message = "server.describe"
    result = enables_signature(cmdline_factory(config, [message, "--serverid", "1"]), message)
    expected = {'serverid': 1}
    assert expected == result


def test_list(api_v1):
    config = Config(api_v1)
    message = "server.list"
    result = enables_signature(cmdline_factory(config, [message]), message)
    assert result == {}


def test_list_unwanted_argument(api_v1):
    config = Config(api_v1)
    message = "server.list"
    raises(SystemExit, f"enables_signature(cmdline_factory(config, [message, '--serverid', '1']), message)")


def test_message_invalid_choice_argument(create_zcli_config, api_v1):
    config = Config(api_v1)
    message = '"server.describe"'
    raises(SystemExit, f"enables_signature(cmdline_factory(config, [{message}, '--serverid', '1', '--falsearg', '750']), {message})")


def test_authenticate(create_zcli_config, mocker, monkeypatch):
    util_authenticate(mocker, monkeypatch, {"access_token": TOKEN})
    assert authenticate(URL) == TOKEN
    with open(token_file, 'r') as t_file:
        assert t_file.read() == TOKEN

    if token_file.is_file():
        token_file.unlink()


def test_authenticate_empty(create_zcli_config, mocker, monkeypatch):
    util_authenticate(mocker, monkeypatch, {})
    assert authenticate(URL) == ''
    with open(token_file, 'r') as t_file:
        assert t_file.read() == ''

    if token_file.is_file():
        token_file.unlink()


def util_authenticate(mocker, monkeypatch, response_return_value):
    if token_file.is_file():
        token_file.unlink()
    monkeypatch.setattr('builtins.input', lambda x: "login")
    mocker.patch.object(getpass, 'getpass')
    getpass.getpass.return_value = "password"
    mocker.patch.object(requests, 'post')
    requests.post.return_value = Response()
    mocker.patch.object(Response, 'json')
    Response.json.return_value = response_return_value
