#!/bin/sh

BUILD_DIR="./build"
ZDIR=$(pwd)
PYTHONPATH=`pwd`:$PYTHONPATH

BINNAME=zephir-client

if [ ! -d ${BUILD_DIR} ]
then
   mkdir -p ${BUILD_DIR}
fi

cd ${BUILD_DIR}
# retrieve dependencies

git clone https://framagit.org/tiramisu/tiramisu-json-api.git
cd tiramisu-json-api
git checkout 9da87804c288355757cbf2e40f4a6fee2fd74234
python3 setup.py install
cd ..
rm -rf tiramisu-json-api

git clone https://framagit.org/tiramisu/tiramisu-cmdline-parser.git
cd tiramisu-cmdline-parser
git checkout b67a1ec4
python3 setup.py install
cd ..
rm -rf tiramisu-cmdline-parser

cd ${ZDIR}
PYTHONOPTIMIZE=1
pyinstaller ${BINNAME} --clean --onefile --name zcli

