#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

import zephir_client

setup(
        name='zephir_client',
        version=zephir_client.__version__,
        packages=find_packages(),
        author="eole@ac-dijon.fr",
        description="Zephir command-line client.",
	long_description="""
        Command line tool allowing access to Zephir API.
        """,
	include_package_data=True,
	url='https://gitlab.mim.ovh/EOLE/Zephir',

	classifiers=[
	    "Programming Language :: Python",
	    "Development Status :: 1 - Planning",
	    "License :: LGPLv3",
	    "Natural Language :: French",
	    "Operating System :: OS Independent",
	    "Programming Language :: Python :: 3",
	    "Topic :: Communications",
	    ],
)
