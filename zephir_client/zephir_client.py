# -*- coding: utf-8 -*-
"""Zephir Client library
"""
import sys
from os import remove
import getpass
from os.path import isfile, basename, expanduser, join, expanduser
from json import dumps, loads
import requests
import warnings
from urllib3.exceptions import InsecureRequestWarning
from argparse import RawDescriptionHelpFormatter

try:
    input = raw_input
except NameError:
    pass

try:
    from tiramisu_api import Config
    from tiramisu_cmdline_parser import TiramisuCmdlineParser
except:
    Config = type('Config')

token_file = join(expanduser("~"), '.zephir-client.jwt.token')

warnings.simplefilter('ignore', InsecureRequestWarning)


def enables_signature(config, message):
    "enables the signature's validation of the remote message"
    payload = {}
    if message:
        # retrieve all arguments, version, returns, ... for selected message
        message_od_name = message
        for option in config.option(message_od_name).list():
            name = option.option.name()
            if name.endswith('.version'):
                api_version = option.value.get()
            elif not option.owner.isdefault() and not option.option.issymlinkoption():
                payload[name] = option.value.get()
    return payload

def list_messages(config, start=None):
    """lists all available messages from config
    """
    messages = config.option('message').value.list()
    if start is not None:
        return [x for x in messages if x.startswith(start)]
    else:
        return messages

def describe_messages(config, start=None):
    """Describes all available messages from config
    """
    messages = config.option('message').value.list()
    descr = []
    for msg in messages:
        # activate each message to enable matching options
        config.option('message').value.set(msg)
        # retrieve message doc in matching optiondescription
        try:
            doc = list(config.option.list(type='all'))[1].option.doc()
        except:
            doc = '!No description available!'
        if start is None or msg.startswith(start):
            descr.append("{msg}\n    {doc}".format(msg,doc))
    return descr

def send(url, version, config):
    # check authentication
    # retrieve new message value
    # (which is the name of message selected by the user)
    message = config.option('message').value.get()
    payload = enables_signature(config, message)
    return send_data(message, url, version, payload)


def send_data(message, url, version, payload):
    # get stored token if available
    if isfile(token_file):
        token = open(token_file).read()
    else:
        token = ''
    auth_ok = False
    # loop on call if authentication failed
    while not auth_ok:
        final_url = 'https://{}/api/{}/{}'.format(url, version, message)
        headers = {'Authorization':'Bearer {}'.format(token)}
        #try:
        ret = requests.post(final_url, data=dumps(payload), headers=headers, verify=False)
        if ret.status_code != 200:
            try:
                response = ret.json()
                if 'error' in response and response["error"]["kwargs"].get("reason") and "bearer" in response["error"]["kwargs"]["reason"]:
                    # Token non valid :
                    # {"error": {"uri": "zephir.unknown_error", "kwargs": {"reason": "unexpected bearer format"}, "description": "unknown error"}}
                    token = authenticate(url)
                    continue
                err = response['error']['kwargs']['reason']
            except:
                import traceback
                traceback.print_exc()
                err = ret.text
            if sys.version_info[0] < 3:
                err = err.encode('utf-8')
            raise Exception('{} ({})'.format(err, ret.status_code))
        response = ret.json()
        auth_ok = True
        if 'error' in response:
            if 'reason' in response['error']['kwargs']:
                raise Exception("{}".format(response['error']['kwargs']['reason']))
            raise Exception('erreur inconnue')
        else:
            return(response['response'])


def authenticate(url, user=None):
    SSO_REALM = "zephir"
    if user is not None:
        USERNAME = user
    else:
        print(u"\nVous n'êtes pas authentifié, veuillez saisir vos identifiants Zéphir\n")
        USERNAME = input("Utilisateur : ")

    if sys.stdin.isatty():
        PASSWORD = getpass.getpass(prompt='Mot de passe : ', stream=None)
    else:
        PASSWORD = sys.stdin.readline().rstrip()

    auth_datas = {
        "username": USERNAME,
        "password": PASSWORD,
        "grant_type": "password",
        "client_id": "zephir-api"
        }
    auth_url = 'https://{}/auth/realms/{}/protocol/openid-connect/token'.format(url, SSO_REALM)
    ret = requests.post(auth_url, data=auth_datas, verify=False)
    if ret.status_code != 200:
        try:
            json = ret.json()
            err = json['error_description']
        except:
            err = ret.text
        raise Exception('{} ({})'.format(err, ret.status_code))
    response = ret.json()
    token = response.get('access_token', '')
    try:
        with open(token_file, 'w') as t_file:
            t_file.write(token)
    except:
        # cannot write token file, proceed anyway
        pass
    return token

def remove_token():
    if isfile(token_file):
        remove(token_file)


def get_error_from_http(req):
    try:
        json = req.json()
        err = json['error']['kwargs']['reason']
    except:
        err = req.text
    return err


def remote_json_to_config(url, headers=None, config_type=Config, subdict=False):
    "retrieves the remote config from the distant api description"
    req = requests.get(url, headers=headers, verify=False)
    if req.status_code != 200:
        err = get_error_from_http(req)
        raise Exception('unable to load url ({}): {} ({})'.format(url,err,req.status_code))
    json = req.json()
    if subdict:
        json = json[subdict]
    return config_type(json)


def cmdline_factory(config, args=None):
    if len(sys.argv) == 2 and (sys.argv[1] == '-h' or sys.argv[1] == '--help'):
        remove_empty_od = False
        progname = basename(sys.argv[0])
        epilog = '''
        {} login [utilisateur]
        Génère un token d'authentification

        {} logout
        Supprime le token d'authentification

        {} --json [--no-pretty] <message>
        Affiche le résultat au format json
                --no-pretty
                    Affiche le résultat json brut

        {} --batch <message> [paramètres]
        Execute le message avec ses paramètres, en mode batch
        '''.format(progname,progname,progname,progname)
    else:
        remove_empty_od = True
        epilog = None
    parser = TiramisuCmdlineParser(config,
                                   fullpath=False,
                                   remove_empty_od=remove_empty_od,
                                   display_modified_value=False,
                                   epilog=epilog,
                                   formatter_class=RawDescriptionHelpFormatter)
    # bypasses mandatory validation in parser
    if args is None:
        parser.parse_args(valid_mandatory=False)
    else:
        parser.parse_args(args, valid_mandatory=False)
    return parser


class ConfigAPI(Config):
    def send_data(self,
                  data):
        ret = None
        for index, payload in enumerate(data['updates']):
            payload['session_id'] = self.session_id
            ret = send_data(self.message, self.url, self.version, payload)
            if ret['status'] == 'error':
                msg = "Impossible de modifier la variable {} : {}".format(payload['name'],ret['message'])
                if self.reporter is not None:
                    self.reporter.setdefault(payload['name'], {})[payload.get('index')] = msg
                else:
                    raise Exception(msg)
        self.updates = []


def configure_server(server, session_id, version, message, reporter=None):
    url = 'https://{}/config/{}/api'.format(server, session_id)
    token = open(token_file).read()
    headers = {'Authorization':'Bearer {}'.format(token)}
    config = remote_json_to_config(url, headers, ConfigAPI)
    config.version = version
    config.message = message
    config.url = server
    config.session_id = session_id
    config.reporter = reporter
    return config


def post_configure_server(server, session_id, payload):
    url = 'https://{}/config/{}/api'.format(server, session_id)
    token = open(token_file).read()
    headers = {'Authorization':'Bearer {}'.format(token)}
    req = requests.post(url, headers=headers, verify=False, data=dumps(payload))
    if req.status_code != 200:
        err = get_error_from_http(req)
        raise Exception('unable to load url ({}): {} ({})'.format(url,err,req.status_code))
    return req.json()
