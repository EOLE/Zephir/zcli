# -*- coding: utf-8 -*-
"""

Zephir command line configuration parser

"""
from os.path import isfile, expanduser, join
import yaml
try:
    input = raw_input
except NameError:
    pass


class ZcliConfig:
    config_file = join(expanduser("~"), '.zephir-config.yaml')

    def __init__(self):
        if not isfile(self.config_file):
            print("""Attention, le fichier de configuration est inexistant !""")
            url = input("Entrez l'adresse du serveur Zephir : ")
            version = input("Entrez la version du zephir (defaut: 'v1') : ")
            if not version:
                version = "v1"
            yaml_template = """url: {}
version: {}""".format(url, version)

            with open(self.config_file, 'w') as fh:
                fh.write(yaml_template)
            print("Le fichier de configuration '.zephir-config.yaml' a été généré dans votre répertoire home. ")

        with open(self.config_file, 'r') as stream:
            try:
                config = yaml.load(stream)
            except yaml.YAMLError as exc:
                print("Erreur au moment du chargement du fichier de configuration", exc)
                config = {}

        self.url = config.get('url')
        self.version = config.get('version')
