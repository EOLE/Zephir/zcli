# Zephir-Client

In order to install or build, you need to have (ubuntu package names are given):

* a working "python3" installation (>= 3.6)
* "python3-requests" : http://docs.python-requests.org/en/master/
* "python3-setuptools" : https://pypi.org/project/setuptools/
* "git" command line utility

# Install zephir-client as python script (recommended) :

* run "sudo ./install.sh"

* This script does install :
  - tiramisu-cmdline-parser and tiramisu-json-api python libraries (cloned from repository)
  - zephir-client python libraries
  - zephir-client python script as /usr/local/bin/zcli

# Build zephir-client binary (alternate method) :

* You need to have "pyinstaller" in your machine (https://www.pyinstaller.org/)

* Start build.sh :
For now this scripts does this :
 - clone Tiramisu-cmd-line parser from https://framagit.org/tiramisu/tiramisu-cmdline-parser.git
 - clone Tiramisu-json-api from https://framagit.org/tiramisu/tiramisu-json-api.git
 - Run pyinstaller to create the "binary" (may fail if pyinstaller not in PATH)

 $ sudo ./build.sh

 This script provides a zcli binary in "dist" directory.
 It can be copied and run on a different system as long as glibc version is compatible

# Usage

* run "zcli -h" (or "zcli --help") for more information.
