#!/bin/sh

########################################################
# Installation script for Zephir command line client.
# requires root privileges for libraries installation.
#
# Script is installed as "zcli" in local bin directory.

set -e

BIN_DIR="/usr/local/bin"
#PYTHONPATH=`pwd`:$PYTHONPATH

BINNAME=zephir-client

pip3 install tiramisu-api==0.4 tiramisu-cmdline-parser==0.4

# install libraries
python3 setup.py install

# install script
cp -rf ${BINNAME} ${BIN_DIR}/zcli
